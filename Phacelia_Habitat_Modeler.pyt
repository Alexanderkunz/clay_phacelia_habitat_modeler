# -*- coding: utf-8 -*-

import arcpy
from arcpy import env
from arcpy import mp
from arcpy.sa import *

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Habitat Modeler 2"
        self.alias = "habmod"

        # List of tool classes associated with this toolbox
        self.tools = [phacelia_tool]


class phacelia_tool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Habitat_Tool"
        self.description = "Model Clay Phacelia Habitat Polygons"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
       # first parameter

        param0 = arcpy.Parameter(
            displayName = 'Data Folder',
            name = 'data_folder',
            datatype = 'DEFolder',
            parameterType = 'Required',
            direction = 'Input')

        # second parameter
        param1 = arcpy.Parameter(
            displayName = 'Habitat Shapefile Name',
            name = 'output_filename',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')

        # third parameter
        param2 = arcpy.Parameter(
            displayName = '10 Meter DEM',
            name = 'dem',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
            
            # second parameter
        param3 = arcpy.Parameter(
            displayName = 'Vegetation Raster',
            name = 'veg',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
            
            # second parameter
        param4 = arcpy.Parameter(
            displayName = 'Geologic Unit Shapefile',
            name = 'geo',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')
        
        params = [param0, param1, param2, param3, param4]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        # get parameters
        data_folder = parameters[0].valueAsText
        output_filename = parameters[1].valueAsText
        dem = parameters[2].valueAsText
        veg = parameters[3].valueAsText
        geo = parameters[4].valueAsText
        
        # In[1]:

        

        mxd = arcpy.mp.ArcGISProject("CURRENT")
        map1 = mxd.listMaps()[0]

        arcpy.env.workspace = data_folder
        arcpy.env.overwriteOutput = True


    # In[2]:

    # In[3]:


        dem_extract = ExtractByAttributes(dem, "VALUE > 1840")
        dem_elev = ExtractByAttributes(dem_extract, "VALUE < 1880")


    # In[4]:


        dem_elev.save("dem_elev.tif")


    # In[5]:


        method = "GEODESIC"
        zUnit = "METER"
        aspect_elev = Aspect("dem_elev.tif", method, zUnit)
        aspect_elev.save("aspect_elev.tif")


    # In[7]:



        aspect_extract = ExtractByAttributes(os.path.join(data_folder, 'aspect_elev.tif'), "VALUE > 67.5")
        aspect_range = ExtractByAttributes(aspect_extract, "VALUE < 157.5")
        aspect_range.save("phacelia_terrain.tif")

    # In[9]:


        terrainRemapRange = RemapRange([[67.5, 157.5, 1]])
        reclass_terrain_raster = Reclassify("phacelia_terrain.tif", "VALUE", terrainRemapRange)

        reclass_terrain_raster.save("reclass_terr.tif")

        outInt = Int(os.path.join(data_folder,"reclass_terr.tif"))
        outInt.save("reclass_terr_int.tif")

        fieldterrain = "Value"
        arcpy.RasterToPolygon_conversion("reclass_terr_int.tif", "terrain_polygon.shp", "NO_SIMPLIFY", fieldterrain)

    # In[12]:


        veg_extract = ExtractByAttributes(veg, "DESCRIPTION LIKE '%Juniper%'")


    # In[13]:


        veg_extract.save("juniper1.tif")


    # In[14]:


        arcpy.RasterToPolygon_conversion(os.path.join(data_folder, "juniper1.tif"), "veg_polygon1.shp")


    # In[15]:


        green_river = arcpy.SelectLayerByAttribute_management(geo, "NEW_SELECTION", "UNITNAME LIKE '%Green River%'")
        arcpy.MakeFeatureLayer_management(green_river, "phacelia_geo.shp", "", data_folder)


    # In[16]:


        arcpy.FeatureClassToFeatureClass_conversion(geo, data_folder, "phacelia_geo.shp", "UNITNAME LIKE '%Green River%'")


    # In[17]:


        inputs = ["terrain_polygon.shp", "veg_polygon1.shp", "phacelia_geo.shp"]
        arcpy.Intersect_analysis(inputs, output_filename)
