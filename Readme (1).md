
This is a tool which is able to automate a process for identifying Clay Phacelia habitat. Clay Phacelia is a rare plant which is only know to grow in specific areas of spanish fork  canyon, as it can only grow under narrow habitat requirements. There have been projects in the past which tried to establish Clay Phacelia populations in new areas, and this tool is a theoretical aid to this process. By using this tool, the process of identifying habitat in a large area is shortened to a few minutes from a much longer time.

Ideal habitat for Clay Phacelia is at elevations from 1840 to 1880 meters elevation, on East and Southeast facing slopes. It prefers soils derived from the Green River Formation, and grows in barren areas between Utah Juniper groves. Teh tool identifies the correct elevation, aspect, vegetation cover, and geologic unit and gives polygons for possible suitable habitat. These polygons would be ideal spots to look for existing populations in Spanish Fork Canyon, or to establish a new population elsewhere.

The tool requires the following data inputs:

- 10 Meter Digital Elevation Model, Utah AGRC (https://gis.utah.gov/data/elevation-and-terrain/)

- SWREGAP landcover class raster layer, swgregap.org (https://swregap.org/data/landcover/)

- A geologic units shapefile (Moab, Nephi, or Price),  Utah AGRC(https://gis.utah.gov/data/geoscience/)

All data will be placed into the specified data folder. Final product will be a shapefile, named by User.

Sample Data is available in the repository.
